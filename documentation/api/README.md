﻿# API Actions
***
## General Information
[Authentication](general/Authentication.md)
***
## Entities
Entity                                                       | URL
------------------------------------------------------------ | ---------------------------
[Tenant](entities/Tenant.md)                                 | /api/tenant
[FrontendUser](entities/FrontendUser.md)                     | /api/frontendUser

## Examples
[Examples](Examples.md)
