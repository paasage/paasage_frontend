
import de.johoop.jacoco4sbt.JacocoPlugin.jacoco

lazy val root = (project in file(".")).enablePlugins(PlayJava)

name := "paasage-frontend"

//disable scala version suffix
crossPaths := false

description := "Rest frontend for the PaaSage Platform"

homepage := Some(url("https://tuleap.ow2.org/plugins/git/paasage/paasage_frontend"))

scmInfo := Some(
  ScmInfo(
    url("https://tuleap.ow2.org/plugins/git/paasage/paasage_frontend.git"),
    "ssh://gitolite@tuleap.ow2.org/paasage/paasage_frontend.git",
    Some("scm:git:gitolite@tuleap.ow2.org/paasage/paasage_frontend.git")
  )
)

organization := "org.ow2.paasage.frontend"

publishMavenStyle := true

version := "2016.4.0-SNAPSHOT"

resolvers += ("ossrh Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots")

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.hibernate" % "hibernate-entitymanager" % "4.3.5.Final",
  cache,
  "org.mariadb.jdbc" % "mariadb-java-client" % "1.1.7",
  "org.hamcrest" % "hamcrest-all" % "1.3",
  "com.google.inject" % "guice" % "3.0",
  "com.google.inject.extensions" % "guice-multibindings" % "3.0",
  "com.google.guava" % "guava" % "18.0",
  "commons-codec" % "commons-codec" % "1.10",
  "com.google.code.findbugs" % "jsr305" % "1.3.9",
  "com.github.drapostolos" % "type-parser" % "0.5.0",
  "org.reflections" % "reflections" % "0.9.10"
)


libraryDependencies ++= Seq(
  "com.github.oxo42" % "stateless4j" % "2.5.0",
  "commons-lang" % "commons-lang" % "2.6",
  "com.rabbitmq" % "amqp-client" %  "3.4.4",
  "org.codehaus.jackson" % "jackson-mapper-asl" % "1.9.13",
  "org.zeromq" % "jeromq" % "0.3.5"
)

TwirlKeys.templateImports += "dtos._"

jacoco.settings

javaOptions in Test += "-Dconfig.file=conf/test.conf"

// we are skipping the components.execution package
// https://issues.scala-lang.org/browse/SI-4744
// causes problem with SimpleFifoPriorityBlockingQueue
// the api-doc task will still generate scala and java doc
// but ignores the problematic files.

scalacOptions in (Compile, doc) := List("-skip-packages",  "components.execution")

pomExtra :=
  <licenses>
    <license>
      <name>The Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>
    <developers>
      <developer>
        <name>Daniel Baur</name>
        <email>daniel.baur@uni-ulm.de</email>
        <organization>University Ulm</organization>
        <organizationUrl>
          https://www.uni-ulm.de/en/in/institute-of-information-resource-management.html
        </organizationUrl>
      </developer>
      <developer>
        <name>Joerg Domaschka</name>
        <email>joerg.domaschka@uni-ulm.de</email>
        <organization>University Ulm</organization>
        <organizationUrl>
          https://www.uni-ulm.de/en/in/institute-of-information-resource-management.html
        </organizationUrl>
      </developer>
      <developer>
        <name>Frank Griesinger</name>
        <email>frank.griesinger@uni-ulm.de</email>
        <organization>University Ulm</organization>
        <organizationUrl>
          https://www.uni-ulm.de/en/in/institute-of-information-resource-management.html
        </organizationUrl>
      </developer>
      <developer>
        <name>Daniel Seybold</name>
        <email>daniel.seybold@uni-ulm.de</email>
        <organization>University Ulm</organization>
        <organizationUrl>
          https://www.uni-ulm.de/en/in/institute-of-information-resource-management.html
        </organizationUrl>
      </developer>
    </developers>

publishTo := {
  val snapshots = "https://oss.sonatype.org/content/repositories/snapshots"
  val releases = "http://repository.ow2.org/nexus/content/repositories/releases/"
  if (version.value.trim.endsWith("SNAPSHOT"))
    Some("snapshots" at snapshots)
  else
    Some("releases" at releases)
}

credentials += Credentials(Path.userHome / ".m2" / ".credentials")


