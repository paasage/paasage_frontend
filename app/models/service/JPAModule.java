/*
 * Copyright (c) 2014-2015 University of Ulm
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package models.service;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import models.*;
import models.generic.ExternalReference;

/**
 * Created by daniel on 18.03.15.
 */
public class JPAModule extends AbstractModule {

    @Override protected void configure() {
        // API Access Token
        bind(ApiAccessTokenRepository.class).to(ApiAccessTokenRepositoryJpa.class);
        //ConstantMonitor
        bind(new TypeLiteral<ModelRepository<ExternalReference>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<ExternalReference>>() {
        });
        // Frontend User
        bind(FrontendUserRepository.class).to(FrontendUserRepositoryJpa.class);
        bind(new TypeLiteral<ModelRepository<FrontendUser>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<FrontendUser>>() {
        });
        // Frontend User Group
        bind(new TypeLiteral<ModelRepository<Tenant>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<Tenant>>() {
        });
        //MeasurementWindow
        bind(new TypeLiteral<ModelRepository<MeasurementWindow>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<MeasurementWindow>>() {
        });
        //ScalingAction
        bind(new TypeLiteral<ModelRepository<ScalingAction>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<ScalingAction>>() {
        });
        //SensorConfigurations
        bind(new TypeLiteral<ModelRepository<SensorConfigurations>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<SensorConfigurations>>() {
        });
        //SensorDescription
        bind(new TypeLiteral<ModelRepository<SensorDescription>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<SensorDescription>>() {
        });
        //TemplateOptions
        bind(new TypeLiteral<ModelRepository<TemplateOptions>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<TemplateOptions>>() {
        });
        //TimeWindow
        bind(new TypeLiteral<ModelRepository<TimeWindow>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<TimeWindow>>() {
        });
        //PaasageModel
        bind(new TypeLiteral<ModelRepository<PaasageModel>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<PaasageModel>>() {
        });
        //Window
        bind(new TypeLiteral<ModelRepository<Window>>() {
        }).to(new TypeLiteral<BaseModelRepositoryJpa<Window>>() {
        });
    }
}
