/*
 * Copyright (c) 2014-2015 University of Ulm
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package models.service;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import models.*;
import models.generic.ExternalReference;

/**
 * Created by daniel on 18.03.15.
 */
public class DatabaseServiceModule extends AbstractModule {

    @Override protected void configure() {

        install(new StaticServiceModule());

        //API Access Token
        bind(ApiAccessTokenService.class).to(DefaultApiAccessTokenService.class);
        // ExternalReference
        bind(new TypeLiteral<ModelService<ExternalReference>>() {
        }).to(new TypeLiteral<BaseModelService<ExternalReference>>() {
        });
        // Frontend User
        bind(FrontendUserService.class).to(DefaultFrontendUserService.class);
        bind(new TypeLiteral<ModelService<FrontendUser>>() {
        }).to(new TypeLiteral<BaseModelService<FrontendUser>>() {
        });
        // Frontend User Group
        bind(new TypeLiteral<ModelService<Tenant>>() {
        }).to(new TypeLiteral<BaseModelService<Tenant>>() {
        });
        //ScalingAction
        bind(new TypeLiteral<ModelService<ScalingAction>>() {
        }).to(new TypeLiteral<BaseModelService<ScalingAction>>() {
        });
        //SensorDescription
        bind(new TypeLiteral<ModelService<SensorConfigurations>>() {
        }).to(new TypeLiteral<BaseModelService<SensorConfigurations>>() {
        });
        //SensorDescription
        bind(new TypeLiteral<ModelService<SensorDescription>>() {
        }).to(new TypeLiteral<BaseModelService<SensorDescription>>() {
        });
        //TemplateOptions
        bind(new TypeLiteral<ModelService<TemplateOptions>>() {
        }).to(new TypeLiteral<BaseModelService<TemplateOptions>>() {
        });
        //TimeWindow
        bind(new TypeLiteral<ModelService<TimeWindow>>() {
        }).to(new TypeLiteral<BaseModelService<TimeWindow>>() {
        });
        //PaasageService
        bind(new TypeLiteral<ModelService<PaasageModel>>() {
        }).to(new TypeLiteral<BaseModelService<PaasageModel>>() {
        });
        //Window
        bind(new TypeLiteral<ModelService<Window>>() {
        }).to(new TypeLiteral<BaseModelService<Window>>() {
        });

    }
}
